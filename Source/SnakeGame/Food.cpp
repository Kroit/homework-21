// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include <ctime>

// Sets default values
AFood::AFood()
{
}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	srand(time(NULL));
	int randlocationX = rand() % 1000 + (-500);
	int randlocationY = rand() % 2000 + (-1000);
	int randlocationZ = 20;
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
			FVector NewLocation(randlocationX, randlocationY, randlocationZ);
			this->SetActorLocation(NewLocation);
			//this->Destroy();
		}
	}
}

