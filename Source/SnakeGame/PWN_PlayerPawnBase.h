// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PWN_PlayerPawnBase.generated.h"

class UCameraComponent;
class ASnakeBase;

UCLASS()
class SNAKEGAME_API APWN_PlayerPawnBase : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APWN_PlayerPawnBase();

	UPROPERTY(BlueprintReadWrite)
		UCameraComponent* PawnCamera;
	UPROPERTY(BlueprintReadWrite)
		ASnakeBase* SnakeActor;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeBase> SnakeActorClass;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void CreateSnakeActor();

	UFUNCTION()
		void HandlerPlayerVerticalInput(float value);
	UFUNCTION()
		void HandlerPlayerHorizontalInput(float value);

	//����� � ������ ����
	float MinY = -480.f;	float MaxY = 480.f;
	float MinX = -480.f;	float MaxX = 480.f;

	//������ ����
	float SpawnZ = 40.f;

	//��������
	void AddRandomFood();
	float StepDelay = 1.f;
	float BuferTime = 0;
};
